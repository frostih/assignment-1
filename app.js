const BankBalanceElement = document.getElementById("bank-balance")
const LaptopsMenuElement = document.getElementById("laptop-menu")
const WorkBalanceElement = document.getElementById("work-balance")
const LoanBalanceElement = document.getElementById("loan-balance")
const LaptopDescriptionElement = document.getElementById("laptop-description")
const LaptopSpecsElement = document.getElementById("laptop-specs")
const LaptopPriceElement = document.getElementById("laptop-price")
const LaptopStockElement = document.getElementById("laptop-stock")
const PhotoElement = document.getElementById("photo")
const BtnWorkElement = document.getElementById("btn-do-work")
const BtnBankElement = document.getElementById("btn-bank")
const BtnGetLoan = document.getElementById("btn-get-loan")
const BtnBuyLaptopElement = document.getElementById("btn-buy-laptop")


// fecth API
async function getPostsAsync(){
    try {
        const response = await fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
        const posts = await response.json()
        return posts
    }
    catch(error){
        console.error("Something went wrong", error)
    }
    finally{
    }

}

// Async for when API data is ready
getPostsAsync()
.then(posts => laptops = posts)
.then(laptops => addLaptopsToMenu(laptops))

// Adding all elements to drop down menu
const addLaptopsToMenu = (laptops) => {
  laptops.forEach(x => addLaptopToMenu(x));
  
  // Initialising info displayed in HTML about laptop
  LaptopDescriptionElement.innerText = laptops[0].description
  for(const x of laptops[0].specs){
    const laptopSpecElement = document.createElement("li")
    laptopSpecElement.appendChild(document.createTextNode(x))
    LaptopSpecsElement.appendChild(laptopSpecElement);
  }
  LaptopStockElement.innerText = laptops[0].stock 
  price = laptops[0].price
  LaptopPriceElement.innerText = Intl.NumberFormat('de-DE', { style: 'currency', currency: 'DKK' }).format(price)
  
  // Initial display of laptop image
  const image = document.createElement("img")
  image.src = laptops[0].image
  PhotoElement.appendChild(image)

}

// Adding each individual element to drop down menu
const addLaptopToMenu = (laptop) => {
  const laptopElement = document.createElement("option")
  laptopElement.value = laptop.id
  laptopElement.appendChild(document.createTextNode(laptop.title))
  LaptopsMenuElement.appendChild(laptopElement);
}

// Initilizing parameters
let bankBalance = 0
let hourlySalary = 100
let debt = 0
let moneyEarned = 0
let price 

// Get paid for work done
function PaySalary() {
  // 10% of money earned goes to debt if there is outstanding debt
  if (debt < 0) { 
    // if 10% of moneyEarned exceeds the value of the debt
    // then the remainder of debt is paid back and not more.
    if (-0.1 * moneyEarned < debt) {
      bankBalance += moneyEarned + debt;
      PayLoan(debt);
    } else {
      bankBalance += 0.9 * moneyEarned;
      PayLoan(0.1 * moneyEarned);
    }
  } else {
    bankBalance += moneyEarned;
  }
  moneyEarned = 0;

  // update innerText with updated info.
  updateHTMLtext()
};

// Get a loan for amount
function Loan (amount) {
  if (debt === 0 && amount <= 2 * bankBalance) {
    debt -= amount;
    bankBalance += amount;
  }

  updateHTMLtext()
};

// Earn money through work
function Work() {
  moneyEarned += hourlySalary;
  updateHTMLtext()

};

// Pay back loan
function PayLoan(amount) {
  if (amount === debt) {
    debt = 0;
  } else {
    debt += amount;
  }
  updateHTMLtext()
};

// updates innerText of HTML with relavent information regarding work, loan and bank balance
function updateHTMLtext() {
  BankBalanceElement.innerText = Intl.NumberFormat('de-DE', { style: 'currency', currency: 'DKK' }).format(bankBalance);
  WorkBalanceElement.innerText =  Intl.NumberFormat('de-DE', { style: 'currency', currency: 'DKK' }).format(moneyEarned);
  LoanBalanceElement.innerText = Intl.NumberFormat('de-DE', { style: 'currency', currency: 'DKK' }).format(debt);
}

updateHTMLtext()

// Handles loan request and check relavent criteria, gives also feedback via alert()
function LoanRequest(){

  let loanAmount = Number(window.prompt("Type a number"))
  if(Object.is(loanAmount, NaN)){
    alert(`Not a valid input`)
  }
  else if (!(debt === 0)){
    alert("You still have an outstanding debt which needs to be paid back before taking another loan.")
  }
  else if (loanAmount > 0 && loanAmount <= 2 * bankBalance){
    Loan(loanAmount)
    alert("Loan granted.")
  }
  else if (loanAmount > 2 * bankBalance){
    alert("That is too much.")
  }
  else{
    alert("That does not make sense!")
  }
}

// Event listeners of buttons
BtnWorkElement.addEventListener("click", Work)
BtnBankElement.addEventListener("click", PaySalary)
BtnGetLoan.addEventListener('click',LoanRequest)

// Handling of changing state of the drop down menu
const handleLaptopMenuChange = e => {
  const selectedLaptop = laptops[e.target.selectedIndex]
  LaptopDescriptionElement.innerText = selectedLaptop.description
  
    // resets the Specs <li> elements of the spec section of laptops
    LaptopSpecsElement.innerText=''
  
    // populates the Specs <li> elements of the spec section of laptops
    for(const x of selectedLaptop.specs){
    const laptopSpecElement = document.createElement("li")
    laptopSpecElement.appendChild(document.createTextNode(x))
    LaptopSpecsElement.appendChild(laptopSpecElement);
  }
  
  // Updates the stock and price innerTexts for laptops of HTML
  LaptopStockElement.innerText = selectedLaptop.stock 
  price = selectedLaptop.price
  LaptopPriceElement.innerText = Intl.NumberFormat('de-DE', { style: 'currency', currency: 'DKK' }).format(price)

    // Display of laptop image according to selsection
    PhotoElement.removeChild(PhotoElement.lastElementChild)
    const image = document.createElement("img")
    image.src = selectedLaptop.image
    PhotoElement.appendChild(image)
}

// Event listener for drop down menu
LaptopsMenuElement.addEventListener('change', handleLaptopMenuChange)

// Handles purchases of laptops, checks price against bank acount and stock
const buyLaptop = () => {
  const selectedLaptop = laptops[LaptopsMenuElement.selectedIndex]
  if(bankBalance>= price && selectedLaptop.stock > 0){
    bankBalance -= price
    selectedLaptop.stock--
    alert(`Congradulations, the "${selectedLaptop.title}" is yours.`)

    updateHTMLtext()
  }
  else if (bankBalance < price){
    alert(`The "${selectedLaptop.title}" laptop is too expensive for you.`)
  }
  else{
    alert(`The "${selectedLaptop.title}" laptop is out of stock.`)
  }
  LaptopStockElement.innerText = selectedLaptop.stock 
}

// Event listener for buy button
BtnBuyLaptopElement.addEventListener('click',buyLaptop)


