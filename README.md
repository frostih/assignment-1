# Assignment 1

Assignment 1 for Noroff .NET course

## Name
Assignment 1 - module 5 .NET course Noroff

## Description
Basic JavaScript project with interaction between .html and .js file.


## Installation
None

## Usage
Run from a web browser.


## Authors and acknowledgment
Frosti

## License
For open source projects, say how it is licensed.

## Project status
Close to finished.
